#!/bin/sh
set -e

if [ -z "$CI_COMMIT_SHA" ]; then
	echo "Error: \$CI_COMMIT_SHA not set"
	exit 1
fi
case "$CI_COMMIT_SHA" in
	*\ * )
		echo "Error: \$CI_COMMIT_SHA contains spaces"
		exit 1
		;;
esac

if [ -z "$TARGET" ]; then
	echo "Error: \$TARGET not set"
	exit 1
fi
case "$TARGET" in  
	*\ * )
		echo "Error: TARGET_DIR contains spaces"
		exit 1
		;;
esac

if [ -z "$DOMAIN" ]; then
	echo "Error: \$DOMAIN not set"
	exit 1
fi
case "$DOMAIN" in  
	*\ * )
		echo "Error: \$DOMAIN contains spaces"
		exit 1
		;;
esac
ID=`date -u  +%Y-%m-%d`_$CI_COMMIT_SHA
echo $ID > id
sed -e "s/@@ID@@/$ID/" -e "s/@@DOMAIN@@/$DOMAIN/" -e "s/@@TARGET@@/$TARGET/" nginx.src > nginx.conf
