#!/bin/sh
set -e

ID=`cat id`
if [ -z "$ID" ]; then
	echo "Error: ID not set"
	exit 1
fi
case "$ID" in  
	*\ * )
		echo "Error: ID contains spaces"
		exit 1
		;;
esac

if [ -z "$TARGET" ]; then
	echo "Error: \$TARGET not set"
	exit 1
fi
case "$TARGET" in  
	*\ * )
		echo "Error: TARGET_DIR contains spaces"
		exit 1
		;;
esac

if [ -z "$DOMAIN" ]; then
	echo "Error: \$DOMAIN not set"
	exit 1
fi
case "$DOMAIN" in  
	*\ * )
		echo "Error: \$DOMAIN contains spaces"
		exit 1
		;;
esac

echo "* Preparing files permissions..."
chmod o-rwx _site nginx.conf
chmod -R ug+rw-x+X _site nginx.conf

echo "* Deploying files..."
ssh www-deploy@$DOMAIN "if [ ! -e /srv/www/$TARGET ] ; then mkdir /srv/www/$TARGET && chgrp www-data /srv/www/$TARGET && chmod 775 /srv/www/$TARGET ; fi"
ssh www-deploy@$DOMAIN "[ ! -e /srv/www/$TARGET/$ID ]"
scp -rp _site www-deploy@$DOMAIN:/srv/www/$TARGET/$ID
ssh www-deploy@$DOMAIN "chgrp -R www-data /srv/www/$TARGET/$ID"

ROLLBACK=`ssh www-deploy@$DOMAIN "readlink /etc/nginx/sites-enabled/$TARGET || echo ''"`
if [ -n "$ROLLBACK" ] ; then
	echo "* Current config is $ROLLBACK"
fi

echo "* Deploying config..."
ssh www-deploy@$DOMAIN "if [ ! -e /etc/nginx/sites-available/$TARGET ] ; then mkdir /etc/nginx/sites-available/$TARGET && chgrp www-conf /etc/nginx/sites-available/$TARGET && chmod 775 /etc/nginx/sites-available/$TARGET ; fi"
ssh www-deploy@$DOMAIN "[ ! -e /etc/nginx/sites-available/$TARGET/$ID ]"
scp -p nginx.conf www-deploy@$DOMAIN:/etc/nginx/sites-available/$TARGET/$ID
ssh www-deploy@$DOMAIN "chgrp www-conf /etc/nginx/sites-available/$TARGET/$ID"

echo "* Testing new config..."
ssh www-deploy@$DOMAIN "ln -snf /etc/nginx/sites-available/$TARGET/$ID /etc/nginx/sites-enabled/$TARGET"
ssh www-deploy@$DOMAIN "chgrp www-conf /etc/nginx/sites-enabled/$TARGET"

if ssh www-deploy@$DOMAIN "sudo nginx -t" ; then
	echo "* Activating..."
	ssh www-deploy@$DOMAIN "sudo systemctl reload nginx" 
	echo "* Deployment successful"
else
	if [ -n "$ROLLBACK" ] ; then
		echo "Failed, rolling back..."
		ssh www-deploy@$DOMAIN "ln -snf $ROLLBACK /etc/nginx/sites-enabled/$TARGET"
		ssh www-deploy@$DOMAIN "chgrp www-conf /etc/nginx/sites-enabled/$TARGET"
		echo "* Deployment failed, rollback successful"
	else
		echo "* Deployment failed"
	fi
	exit 1
fi
