# Continuous Site Deployer

# Intro
I use this small module to set up continous deployement via gitlab for web sites I operate.
This is not expected to work on servers that aren't configured “my way”.

## Usage
* `git submodule add https://gitlab.com/frivoal/www-deployer.git`
* Include a `known_hosts` file in the root of the repo for the server to deploy to
* Copy `nginx.src` to the root of the repo, and customize as appropriate
* Set up your build step so that the site you want to deploy is in `_site`, and that is included in the artifacts
* Set up an `SSH_PRIVATE_KEY` secret varaible for the project on gitlab
* Add `nginx.conf` and `id` to `.gitignore`
* If using jekyll, add `id`, `nginx.src`, `nginx.conf`, `www-deploy`, and `known_hosts` to `_config.yml`
* Include (and customize) the following bits into `.gitlab-ci.yml`

~~~
variables:
  DOMAIN: "somewhere.example.com"
  TARGET: "somewhere"

build:
  [...]
  before_script:
    - apt-get update
    - apt-get upgrade -y ca-certificates
    - git submodule update --init
    [...]
  stage: build
  script:
    - [...]
    - www-deployer/build-conf.sh
  artifacts:
    paths:
      - _site/
      - id
      - nginx.conf

deploy:
  image: debian:stretch
  before_script:
    - apt-get update
    - apt-get upgrade -y ca-certificates
    - apt-get install -y git
    - git submodule update --init www-deployer
    - mkdir ~/.ssh
    - chmod 700 ~/.ssh
    - cp known_hosts ~/.ssh/
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
  stage: deploy
  only:
    - master
  script:
    - www-deployer/deploy.sh
~~~
